#include <iostream>
#include <conio.h>
#include <ctype.h>
#include <Windows.h>
using namespace std;


// Function Declaration__________________________________________________
char oui_non();
int prevision(char obligatoire, char nouveau, int etudiants, int stock);
double marge(int livre, double prix);
void afficher(int bookCode, int bookAmmount, double revenu, int studentNum);
int validInt(int valueInt);
double validDouble(double valueInt);

// Constant Variable Declaration_________________________________________
double bookObliNew = 0.60;
double bookObliOld = 0.85;

double bookReffNew = 0.25;
double bookReffOld = 0.40;

double profitBase = 0.25;
double profitBulk = 0.20;

int profitBulkStart = 10;


// Main Function ________________________________________________________
void main() {

	// TITLE ____________________________________________________________
	SetConsoleTitle(TEXT("TP1_P63"));
	cout << "TP1 Librairie Universitaire" << endl;
	cout << "___________________________" << endl;
	cout << " " << endl;

	// VARIABLE DECLARATION _____________________________________________
	// user input variables
	int  bookNumb = 0;
	int  bookStock = 0;
	char bookRequ = ' ';
	char bookWorn = ' ';
	double bookPrice = 0;
	int studentNum = 0;

	// Calculation Variables
	int bookPrediction = 0;
	double bookProfit = 0;

	// USER INPUT _______________________________________________________
	do {
		
			cout << "Sil vous plait entrer le id du livre    (entrer -1 pour terminer)" << endl;
			bookNumb = validInt(bookNumb);
			cout << endl;
		

		if (bookNumb < -1) {
			do {
				cout << "Sil vous plait entrer un nombre positif    (entrer -1 pour terminer)" << endl;
				bookNumb = validInt(bookNumb);
				cout << endl;
			} while (bookNumb < -1);
		}
		
		if (bookNumb == -1) {
			break;
		}

		cout << "Combien de livre avez-vous en stock presentement?" << endl;
		bookStock = validInt(bookStock);
		cout << endl;

		cout << "Esceque ce livre est obligatoire? o/n" << endl;
		bookRequ = oui_non();
		cout << endl;

		cout << "Esceque ce livre a deja ete utiliser? o/n" << endl;
		bookWorn = oui_non();
		cout << endl;

		cout << "Combien coute ce livre?" << endl;
		bookPrice = validDouble(bookPrice);
		cout << endl;

		cout << "Combien detudiant inscrit au cour?" << endl;
		studentNum = validInt(studentNum);
		cout << "_________________________________________________" << endl;

		//	CALCULATION ______________________________________________________
		bookPrediction = prevision(bookRequ, bookWorn, studentNum, bookStock);
		bookProfit = marge(bookPrediction, bookPrice);

		//	RESULT ___________________________________________________________
		afficher(bookNumb, bookPrediction, bookProfit, studentNum);
		cout << "_________________________________________________" << endl;
		cout << endl;

		bookNumb = 0;
	} while (bookNumb != -1);
	
	cout << "Merci davoir utiliser notre programme ^_^" <<endl;
	cout << "_________________________________________________" << endl;
}


char oui_non() {
	char choice = ' ';

	do {
		cin >> choice;
		cin.get();
		choice = toupper(choice);


		if ((choice != 'O') && (choice != 'N')) {
			cout << "vous devez entrer o/n" << endl;
		}
	} while ((choice != 'O') && (choice != 'N'));

	return choice;
}


int prevision(char obligatoire, char nouveau, int etudiants, int stock) {
	double bookPredic = 0;
	int bookTotal = 0;

	if (obligatoire == 'O') {
		if (nouveau == 'O') {
			bookPredic = bookObliNew;
		}
		else {
			bookPredic = bookObliOld;
		}
	}
	else {
		if (nouveau == 'O') {
			bookPredic = bookReffNew;
		}
		else {
			bookPredic = bookReffOld;
		}
	}
	bookTotal = (int)(etudiants * bookPredic) - stock;
	return bookTotal;
}


double marge(int livre, double prix) {
	double profit = 0;

	if (prix < profitBulkStart) {
		profit = profitBase;
	}
	else {
		profit = profitBulk;
	}
	return (livre * prix) * profit;
}


void afficher(int bookCode, int bookAmmount, double revenu, int studentNum) {
	cout << "Code du livre : " << bookCode << endl;

	if (bookAmmount >= 1) {
		cout << "Livre a commander : " << bookAmmount << endl;
	}
	else if (bookAmmount < 0) {
		bookAmmount = bookAmmount * -1;
		cout << "Livre a retourner : " << bookAmmount << endl;
	}
	else {
		cout << "Aucun livre a commander" << endl;
	}

	if (revenu > 1) {
		cout << "Profit total : " << revenu << "$" << endl;
	}
	else if (revenu <= 0) {
		cout << "Appeller le fournisseur pour connaitre les frais de retour" << endl;

	}
	if (studentNum < 0){
		cout << "Le nombre d'�tudiants est n�gatif et donc impertinant pour les calculs de cette application" << endl;
	}
}


int validInt(int valueInt) {
	cin >> valueInt;

	while (cin.fail() || cin.peek() != '\n') {
		cout << "Entrer non valide" << endl;
		cin.clear();
		cin.ignore(512, '\n');
		cin >> valueInt;
	}
	return valueInt;
}


double validDouble(double valueInt) {
	cin >> valueInt;

	while (cin.fail() || cin.peek() != '\n') {
		cout << "Entrer non valide" << endl;
		cin.clear();
		cin.ignore(512, '\n');
		cin >> valueInt;
	}
	return valueInt;
}