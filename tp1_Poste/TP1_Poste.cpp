#include <iostream>
#include <sstream>
#include <iomanip>
#include <stdio.h>
#include <conio.h>
#include <Windows.h>
using namespace std;


/*
Analyse

1) Entr�es le poids et cumuler les resultats

2) Verifier le tier de poid, l'associer au tier de prix et cummuler

3) A chaque entr� le nombre de lettre augmente de 1

4) User entre le nombre 0 pour avoir une sorti

5) Les traitements:
-SI prix de cummulation total est plus grand que 20$ donner un rabait de 10%
-Prix de base moin le rabait(si applicable) plus les taxes egal prix total

6) Affiche:
-total de lettre envoy�
-total de taxe pay�
-total prix final
-total economis� SI applicable
*/

// Function Declaration ________________________________________
int validInt(int valueInt);

// Constant Variable Declaration _______________________________
// weight classification per letter
const int weightT1 = 27;
const int weightT2 = 81;
const int weightT3 = 120;
const int weightT4 = 121;

// cost of letter based on classification
const double priceT1 = 0.45;
const double priceT2 = 0.90;
const double priceT3 = 1.10;
const double priceT4 = 1.50;

// discount parameter
const double priceTresh = 20.0;
const double priceReduc = 0.10;

// tax parameter
const double tps = 0.05; //Tax
const double tvq = 0.10; //Tax



// Main Function
void main() {
	// TITLE ____________________________________________________________
	SetConsoleTitle(TEXT("TP1_P63"));
	cout << "TP1 Bureau de poste Saint-Profond-Des-Creux" << endl;
	cout << "___________________________________________" << endl;
	cout << " " << endl;

	// VARIABLE DECLARATION _____________________________________________
	// user input variables
	int weight = 0;
	int letter = 0;

	// Control Variable
	int weightInvalid = 0;

	// Calculation Variables
	double priceBase = 0;
	double priceOff = 0;
	double priceTaxes = 0;
	double priceTotal = 0;

	//Validation Saison
	bool saisieCorrecte = false;


	// USER INPUT _______________________________________________________
	// user weight input

	do { //1�re condition autre que 0
		do { //2ieme condition entree valide et positive
			cout << "Veuiller saisir le poids ? " << endl; //Consigne de saisie sans erreur
			cin >> weight; //Saisie
			cout << endl;

			//***************VALIDATION******************
			if (cin.fail() || cin.peek() != '\n') { //Si n'est pas un entier
				cin.clear();
				cin.ignore(512, '\n');
				cout << "Erreur 1 | Veuiller entrez un poids valide" << endl;
				cout << endl;
				weightInvalid++;
			}
			else if (weight < 0){ //Si n'est pas positif
				cin.clear();
				cin.ignore(512, '\n');
				cout << "Erreur2 | Veuiller entrez un poids positif" << endl;
				cout << endl;
				weightInvalid++;
			}
			else  {
				saisieCorrecte = true;
			}
			
			//***************Calcul� si valide*******************
			if (weight != 0 && saisieCorrecte ==true){
				if (weight < weightT1) {
					priceBase += priceT1;
				}
				else if (weight < weightT2) {
					priceBase += priceT2;
				}
				else if (weight < weightT3) {
					priceBase += priceT3;
				}
				else if (weight < weightT4) {
					priceBase += priceT4;
				}
				// Letter Cummulation
				letter++;
			}

		} while (!saisieCorrecte);
	} while (weight != 0);


	// CALCULATIONS ____________________________________________________
	// check if discount apply
	if (priceBase > priceTresh) {
		priceOff = priceBase - priceTresh;
		priceOff = priceOff  * priceReduc;
	}

	// Tax Calculation
	priceTaxes = (priceBase - priceOff) * (tps + tvq);

	// Total Calculation
	priceTotal = priceBase - priceOff + priceTaxes;
	cout << "___________________________________________" << endl;


	// RESULTS _________________________________________________________
	// single or multiple letter sent
	cout << "Vous avez envoyer " << letter;

	if (letter <= 1) {
		cout << " lettre" << endl;
	}
	else {
		cout << " lettres" << endl;
	}
	cout << "___________________________________________" << endl;

	// Tax Price
	cout << "Vous payez " << setprecision(2) << fixed << priceTaxes << "$ en taxe" << endl;

	// Final Price
	cout << "Le prix total est de " << setprecision(2) << fixed << priceTotal << "$" << endl;

	// Check if you qualify for discount
	if (priceBase > priceTresh) {
		cout << "Economie de " << setprecision(2) << fixed << priceOff << "$" << endl;
	}
	cout << "___________________________________________" << endl;

	if (weightInvalid > 0) {
		cout << "Vous-avez entrer " << weightInvalid << " poids invalide(s)" << endl;
		cout << "___________________________________________" << endl;
		cout << endl;
	}
}

